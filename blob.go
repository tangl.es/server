package tangles

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
)

// A Blob represents an opaque binary set of data. It does not contain the data
// in question, but does contain the metadata about the data.
type Blob struct {
	SHA256      string
	Size        int64
	ContentType string
}

// StreamingBlobUpload reads a blob from src and writes it to dst without reading
// the entire blob into memory first. It returns the blob that was uploaded or an
// error.
//
// StreamingBlobUpload calculates the sha256 of the incoming stream, but it is the
// job of the caller to verify that the sha256 matches what the uploader says it
// should be. A mismatch is either invalid input or a corrupted upload.
//
// It is also worth noting that StreamingBlobUpload will blindly overwrite data if
// it is given a hash that already exists in the blobstore; hashes that already
// exist in the blobstore should be ignored before reaching StreamingBlobUpload.
func StreamingBlobUpload(contentType string, src io.Reader, dst io.Writer) (Blob, error) {
	if rc, ok := src.(io.ReadCloser); ok {
		defer rc.Close()
	}
	sha := sha256.New()
	w := io.MultiWriter(sha, dst)
	size, err := io.Copy(w, src)
	if err != nil {
		return Blob{}, err
	}
	return Blob{
		ContentType: contentType,
		SHA256:      hex.EncodeToString(sha.Sum(nil)),
		Size:        size,
	}, nil
}
